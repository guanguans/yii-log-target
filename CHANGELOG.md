# Changelog

All notable changes to `guanguans/yii-log-target` will be documented in this file.

## 1.0.0 - 2021-05-18

* Initial release.
